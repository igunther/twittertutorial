//
//  AppDelegate.swift
//  TwitterTutorial
//
//  Created by Øystein Günther on 31/10/2020.
//

import UIKit
import SwiftyBeaver
import Firebase

let swiftyBeaverLog = SwiftyBeaver.self

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initSwiftyBeaverLog()
        FirebaseApp.configure()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func initSwiftyBeaverLog() {
        let console = ConsoleDestination()
        let file = FileDestination()
        swiftyBeaverLog.addDestination(console)
        swiftyBeaverLog.addDestination(file)
        
        let platform = SBPlatformDestination(appID: "x7PvRV", appSecret: "bau7p3go0ckamsgqoEbgylrp00aZt401", encryptionKey: "fdsf8fqi8aeomhiicrRtkBf04yn6uQ3k")
        
        swiftyBeaverLog.addDestination(platform)
    }
}

