//
//  ConversationsController.swift
//  TwitterTutorial
//
//  Created by Øystein Günther on 01/11/2020.
//

import UIKit

class ConversationsController: UIViewController {
    
    // MARK: Properties
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }

    // MARK: Helpers

    func configureUI() {
        view.backgroundColor = .white
        navigationItem.title = "Messages"
    }
}

