//
//  MainTabController.swift
//  TwitterTutorial
//
//  Created by Øystein Günther on 31/10/2020.
//

import UIKit
import Firebase

class MainTabController: UITabBarController {

    // MARK: Properties
    
    let actionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tweet"), for: .normal)
        //button.backgroundColor = .twitterBlue
        //button.tintColor = .white
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //logUserOut()
        view.backgroundColor = .twitterBlue
        authenticateUserAndConfigureUI()
    }

    // MARK: API
    
    func authenticateUserAndConfigureUI() {
        if Auth.auth().currentUser == nil {
            swiftyBeaverLog.debug("User is not logged in")
            DispatchQueue.main.async {
                let nav = UINavigationController(rootViewController: LoginController())
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        } else {
            configureViewControllers()
            configureUI()
        }
    }
    
    func logUserOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            swiftyBeaverLog.error("Failed to log out user with error: \(error.localizedDescription)")
        }
    }
    
    // MARK: Selectors
    
    @objc func actionButtonTapped() {
        print(123)
    }
        
    // MARK: Helpers
    
    func configureUI() {
        view.addSubview(actionButton)
        actionButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingBottom: 64, paddingRight: 16, width: 54, height: 54)
        actionButton.layer.cornerRadius = 54 / 2
    }
    
    func configureViewControllers() {
        
        let feed = FeedController()
        let nav1 = templateNavigationController(image: UIImage(named: "tabBarFeed"), rootViewController: feed)
        
        let explore = ExploreController()
        let nav2 = templateNavigationController(image: UIImage(named: "tabBarExplore"), rootViewController: explore)
                
        let notifications = NotificationsController()
        let nav3 = templateNavigationController(image: UIImage(named: "tabBarNotifications"), rootViewController: notifications)
                
        let conversations = ConversationsController()
        let nav4 = templateNavigationController(image: UIImage(named: "tabBarConversations"), rootViewController: conversations)
        
        viewControllers = [nav1, nav2, nav3, nav4]
    }
    
    func templateNavigationController(image: UIImage?, rootViewController: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.tabBarItem.image = image
        nav.navigationBar.barTintColor = .white
        nav.tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        return nav
    }
}
